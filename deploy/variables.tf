variable "prefix" {
  default = "tfdevops"
}

variable "project" {
  default = "terraform-devops"
}

variable "contact" {
  default = "me@example.com"
}

variable "db_username" {
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "601912882130.dkr.ecr.eu-central-1.amazonaws.com/terraform-devops-repo:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for API"
  default     = "601912882130.dkr.ecr.eu-central-1.amazonaws.com/terraform-devops-repo:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "quiero.ch"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}

