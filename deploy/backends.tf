terraform {
  backend "s3" {
    bucket         = "terraform-devops-601912882130-tfstate"
    key            = "terraform-devops.tfstate"
    region         = "eu-central-1"
    encrypt        = true
    dynamodb_table = "terraform-devops-601912882130-tfstate-lock"
  }
}
