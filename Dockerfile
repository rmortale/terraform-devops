FROM nginxinc/nginx-unprivileged:1-alpine
LABEL maintainer="maintainer@londonappdev.com"


COPY index.html /usr/share/nginx/html/index.html
USER root
COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
USER nginx
CMD ["/entrypoint.sh"]
